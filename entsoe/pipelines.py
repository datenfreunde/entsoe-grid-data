# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import logging
logger = logging.getLogger(__name__)


from ocbclient import Orion

from configuration import Config as config


class NotifyLastActualLoad(object):


    def open_spider(self, spider):
        self.store = None

    def close_spider(self, spider):
        orion = Orion(config.OUTPUT["broker"], Service=config.OUTPUT["service"])
        logger.debug(f"storing {self.store}")
        for (k,v) in config.OUTPUT["mapping"].items():
            orion[k] = self.store[v]
            logger.info(f"stored v {self.store[v]} in {orion}{k}")

    def process_item(self, item, spider):
        if 'production' in item:
            self.store = item
        return item


