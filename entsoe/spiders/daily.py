import scrapy
from scrapy.loader import ItemLoader
from scrapy.loader.processors import MapCompose, TakeFirst
import datetime
from itertools import chain
import re
import os

_HERE = os.path.split(__file__)[0]

_ABS = '/home/martin/Dropbox/ometepe/so-power-generation-data'
DAILY = [

    "https://transparency.entsoe.eu/load-domain/r2/totalLoadR2/show?name=&defaultValue=false&viewType=TABLE&areaType=CTY&atch=false&dateTime.dateTime={now:%d.%m.%Y}+00:00|CET|DAY&biddingZone.values=CTY|10Y1001A1001A83F!CTY|10Y1001A1001A83F&dateTime.timezone=CET_CEST&dateTime.timezone_input=CET+(UTC+1)+/+CEST+(UTC+2)#"
    # f'file://{_ABS}/dateTime=02.10.2018-forecast-prod.html',
    #f'file://{_ABS}/dateTime=03.10.2018-forecast-prod.html'
    ]

def table_format(a, urlday=None):
    if a == "-" :
        return None
    else:
        try:
            return int(a)
        except ValueError:
            return urlday+a[1:6]+":00"

def int_or_none(a):
    try:
        return int(a)
    except ValueError:
        return None

def parse_date(text, loader_context):
    date = loader_context["date"]
    frag = text[0][1:6]
    return f"{date}{frag}:00"

class EnergyProduction(scrapy.Item):

    timestamp = scrapy.Field(
            output_processor = parse_date
    )

    forecast = scrapy.Field(
            input_processor = MapCompose(int_or_none),
            output_processor = TakeFirst()
    )
    production = scrapy.Field(
            input_processor = MapCompose(int_or_none),
            output_processor = TakeFirst()
    )






class TableSpider(scrapy.Spider):

    name = "daily"


    def start_requests(self):
        for u in DAILY:
            yield scrapy.Request(u.format(now=datetime.datetime.now()))



    def parse(self, response):
        o = re.search("dateTime=(\d\d.\d\d.\d\d\d\d)",response.url)
        urlday = datetime.datetime.strptime(o.groups()[0],"%d.%m.%Y").strftime("%Y-%m-%dT")
        for row in response.css("tr"):
            # import IPython; IPython.embed()
            il = ItemLoader(item=EnergyProduction(), selector=row, date=urlday)
            il.add_css('timestamp', "td::text")
            il.add_css('forecast', "td:nth-child(2) span::text")
            il.add_css('production', "td:nth-child(3) span::text")
            yield il.load_item()


    FEED_URI    = "file:///tmp/export.jl"
    FEED_FORMAT = "jsonlines"


