import logging
logger = logging.getLogger(__name__)


class Config:



    OUTPUT = {
              "broker"  : "http://81.14.203.235:1026",
              "service" : "hacki1",
              "mapping" : {
                            'LoadForecast/attrs/MW/value' : 'forecast',
                            'ActualLoad/attrs/MW/value' : 'production'
                         }
             }



    SPIDER = {

                'interval' : 60*5 # five minutes }

             }


    @classmethod
    def update_from_dict(cls,dic):
        logger.debug(f"Updating {cls}")
        for (k,v) in dic.items():
            parts = k.split("_")
            if len(parts) == 2:
                a = parts[0].upper()
                n = parts[1].lower()
                if hasattr(cls, a) and n in getattr(cls, a):
                    t = type(getattr(cls,a)[n])
                    if t in (dict, list):
                        try:
                            v = eval(v)
                        except Exception as e:
                            raise ValueError(f"{a}[{n}] = {v} failed: {e}")
                    elif t in (int,float):
                        v = t(v)
                    getattr(cls,a)[n]=v
                    logger.debug(f"{k} {v} overriding {a}[{n}]")







scrapy_settings = dict(
                BOT_NAME = 'entsoe',
                SPIDER_MODULES = ['entsoe.spiders'],
                NEWSPIDER_MODULE = 'entsoe.spiders',
# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'entsoe (+http://www.yourdomain.com)'

# Obey robots.txt rules
                ROBOTSTXT_OBEY = True,

# Configure maximum concurrent requests performed by Scrapy (default: 16)
#CONCURRENT_REQUESTS = 32

# Configure a delay for requests for the same website (default: 0)
# See https://doc.scrapy.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
#DOWNLOAD_DELAY = 3
# The download delay setting will honor only one of:
#CONCURRENT_REQUESTS_PER_DOMAIN = 16
#CONCURRENT_REQUESTS_PER_IP = 16

# Disable cookies (enabled by default)
#COOKIES_ENABLED = False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED = False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See https://doc.scrapy.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'entsoe.middlewares.EntsoeSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    'entsoe.middlewares.EntsoeDownloaderMiddleware': 543,
#}

# Enable or disable extensions
# See https://doc.scrapy.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.extensions.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See https://doc.scrapy.org/en/latest/topics/item-pipeline.html
#ITEM_PIPELINES = {
#    'entsoe.pipelines.EntsoePipeline': 300,
#}

# Enable and configure the AutoThrottle extension (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/autothrottle.html
#AUTOTHROTTLE_ENABLED = True
# The initial download delay
#AUTOTHROTTLE_START_DELAY = 5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY = 60
# The average number of requests Scrapy should be sending in parallel to
# each remote server
#AUTOTHROTTLE_TARGET_CONCURRENCY = 1.0
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG = False

# Enable and configure HTTP caching (disabled by default)
# See https://doc.scrapy.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED = True
#HTTPCACHE_EXPIRATION_SECS = 0
#HTTPCACHE_DIR = 'httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES = []
#HTTPCACHE_STORAGE = 'scrapy.extensions.httpcache.FilesystemCacheStorage'

            ITEM_PIPELINES = {
                    'entsoe.pipelines.NotifyLastActualLoad': 300
                    # 'pipelines.NotifyLastActualLoad': 300
                    #'unknown' : 100
            },

            FEED_URI    = "file:///tmp/export.jl",
            FEED_FORMAT = "jsonlines",
            FEED_STORE_EMPTY = True
)


