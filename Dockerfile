
# FROM mielune/alpine-python3-arm:edge
FROM python:3.6-alpine

ENV LANG en_US.UTF-8

RUN mkdir /task

WORKDIR /task

COPY requirements.txt /task

RUN apk add --no-cache libxml2 libxslt &&\
    apk add --virtual .build-tools --no-cache libffi-dev openssl-dev libxml2-dev libxslt-dev git build-base &&\
    pip3 install --upgrade pip  &&\
    pip3 install -r requirements.txt &&\
    apk del .build-tools


COPY . /task





