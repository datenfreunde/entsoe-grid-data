SHELL := /bin/bash


IMAGE_NAME := power_grid_data


requirements.txt requirements-dev.txt: Pipfile Pipfile.lock
	pipenv run pipenv_to_requirements -f ;\
	sed -i /ocbclient/d requirements.txt ;\
	echo 'git+https://git@bitbucket.org/datenfreunde/ocbclient.git@v0.0.4#egg=ocbclient' >>requirements.txt



build: requirements.txt requirements-dev.txt
	docker build . -t $(IMAGE_NAME)


run-once:
	docker run --rm --env SPIDER_INTERVAL=0 --env PYTHON_LOG_LEVEL=DEBUG -it $(IMAGE_NAME) python3 runspider.py

run-forever:
	docker run --rm --env SPIDER_INTERVAL=300 --env PYTHON_LOG_LEVEL=INFO -it $(IMAGE_NAME) python3 runspider.py
