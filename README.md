# Real-Time Power Grid data

This repo provides a way of getting power grid data as a service in the context of the [SmartOrchestra Project](smarto).

The data is read from the Entsoe Transparency Website and forwarded to a [Orion Context Broker](https://fiware-orion.readthedocs.io/en/master/).


## How to build & run the docker container

This project uses Pipenv. The `requirements.txt` file for the Docker container is generated from the `Pipfile`. 

1. Install the python environment

```
pipenv --python 3.6 install --dev 
```

1. Build the image.

```
make build
```


To run once:
	
```    
    docker run --rm --env SPIDER_INTERVAL=0 --env PYTHON_LOG_LEVEL=DEBUG -it $(IMAGE_NAME) python3 runspider.py
``` 

To run the service every 5 Minutes:
	
```    
    docker run --rm --env SPIDER_INTERVAL=300 --env PYTHON_LOG_LEVEL=DEBUG -it $(IMAGE_NAME) python3 runspider.py
``` 
### How to configure the Docker container

Configuration happens in configuration.py, it can be changed using environment variables, e.g. 
`OUTPUT_BROKER=http://orion.hosted.elsewhere.com`.

The following variables that can be used:

    - OUTPUT_BROKER
    - OUTPUT_SERVICE
    - OUTPUT_MAPPING


Also, PYTHON_DEBUG_LEVEL can be set to the usual values, e.g. DEBUG, LOG, INFO or ERROR.

Here's an example:

```

    OUTPUT = {
              "broker"  : "http://81.14.203.235:1026",
              "service" : "hacki1",
              "mapping" : {
                            'LoadForecast/attrs/MW/value' : 'forecast',
                            'ActualLoad/attrs/MW/value' : 'production'
                         }
             }

```

This configuration will write to the service `hacki1` on broker at `http://81.14.203.235:1026` and 
set the attribute `MW` of `LoadForcast` and `ActualLoad` to the most recent available values
of `forecast` and `production`, respectively.

Forecast and Production are taken from this table:


    https://transparency.entsoe.eu/load-domain/r2/totalLoadR2/show?name=&defaultValue=false&viewType=TABLE&areaType=CTY&atch=false&dateTime.dateTime={now:%d.%m.%Y}+00:00|CET|DAY&biddingZone.values=CTY|10Y1001A1001A83F!CTY|10Y1001A1001A83F&dateTime.timezone=CET_CEST&dateTime.timezone_input=CET+(UTC+1)+/+CEST+(UTC+2)#


The URL is configured in `entsoe/spiders/daily.py`. 



## Notes

Entsoe granted API access, which is not used in this project because it reamained unclear how to access the data.


## References 

  - [ENTSOE Api](https://transparency.entsoe.eu/content/static_content/Static%20content/web%20api/Guide.html)

(c) 2016-2018 Datenfreunde GmbH

This module is part of the [Smart Orchestra Project](smarto), co-financed by the
[Federal Ministery of Economics and Technology](bmwi).


[smarto]: http://smartorchestra.de

[bmwi]: https://www.bmwi.de/

[git]: https://bitbucket.org/datenfreunde/entsoe-grid-data/src/master/

