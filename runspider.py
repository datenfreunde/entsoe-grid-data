import sys,os
sys.path.append(os.path.split(__file__)[0] + "/entsoe")
sys.path.append(os.path.split(__file__)[0] )

import logging

logger = logging.getLogger(__name__)


if "PYTHON_LOG_LEVEL" in os.environ:
    log_level = getattr(logging,os.environ["PYTHON_LOG_LEVEL"].upper())
else:
    log_level = logging.INFO

logging.getLogger().setLevel(log_level)
logging.basicConfig(level=log_level, stream=sys.stderr)




from twisted.internet import reactor, defer
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
import scrapy
import time

from entsoe.spiders.daily import TableSpider
from configuration import scrapy_settings

# configure_logging()
runner = CrawlerRunner(scrapy_settings)

@defer.inlineCallbacks
def crawl(config):
    forever = True
    while forever:
        try:
            yield runner.crawl(TableSpider)
            if config.SPIDER["interval"] > 0:
                logger.info(f"Waiting {config.SPIDER['interval']} seconds")
                time.sleep(config.SPIDER["interval"])
            else:
                logger.info(f"Exiting, SPIDER_INTERVAL is {config.SPIDER['interval']}")
                forever = False
        except Exception as e:
            logger.exception(e)
            forever = False
    reactor.stop()



def run(config):
    crawl(config)
    reactor.run()


if __name__=='__main__':
    from configuration import Config
    logger.info("Starting...")
    Config.update_from_dict(os.environ)
    run(Config)


